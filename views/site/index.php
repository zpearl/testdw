<?php
/* @var $this yii\web\View */

use yii\widgets\Pjax;
use yii\bootstrap\Html;

$text1 = Yii::$app->session->get('text1');
$text2 = Yii::$app->session->get('text2');
$text3 = Yii::$app->session->get('text3');

$this->title = 'My Yii Application';
Pjax::begin([
    'enablePushState'    => false,
    'enableReplaceState' => false,
]);

echo Html::a($text1, ['index', 'update' => true])." ".Html::a($text2, ['index', 'update' => true])." ".Html::a($text3,
    ['index', 'update' => true]);
?>

<?php Pjax::end() ?>
